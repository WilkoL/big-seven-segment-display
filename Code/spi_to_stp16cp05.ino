
#include <SPI.h>




void stp16cp05_show_word(uint8_t chr1, uint8_t chr2, uint8_t chr3, uint8_t chr4);
void stp16cp05_show_number(uint16_t number);
uint16_t conv_to_seven_segment(uint8_t digit);
void stp16cp05_send(uint16_t data);
void stp16cp05_latch(void);


//0 1 2 3 4 5 6 7 8 9
//A b C d E F H i J L
//o P r S t U Z - _ °  space

uint16_t table[31] =
{
  0x3f7e,	//0		0
  0x0360,	//1		1
  0x0f9f,	//2		2
  0x0ff9,	//3		3
  0x33e1,	//4		4
  0x3cf9,	//5		5
  0x3cff,	//6		6
  0x0f60,	//7		7
  0x3fff,	//8		8
  0x3ff9,	//9		9
  0x3fe7,	//10	A
  0x30ff,	//11	b
  0x3c1e,	//12	C
  0x03ff,	//13	d
  0x3c9f,	//14	E
  0x3c87,	//15	F
  0x33e7,	//16	H
  0x0260,	//17	i
  0x0378,	//18	J
  0x301e,	//19	L
  0x00ff,	//20	o
  0x3f87,	//21	P
  0x0007,	//22	r
  0x3cf9,	//23	S
  0x309f,	//24	t
  0x337e,	//25	U
  0x0f9f,	//26	Z
  0x0081,	//27	-
  0x0018,	//28	_
  0x3f81,	//29	°
  0x0000	//30	space
};
uint16_t teller = 0;


void setup()
{
  const int slave_select_pin = 10;
  const int latch_pin = 8;

  pinMode(slave_select_pin, OUTPUT);
  pinMode(latch_pin, OUTPUT);
  SPI.begin();

  //stp16cp05_show_word(16, 14, 19, 21);		//HELP
  //delay(4000);
}

void loop()
{
  stp16cp05_show_number(teller++);
  if (teller > 9999) teller = 0;
  delay(10);
}





void stp16cp05_show_number(uint16_t number)
{
  if (number > 9999) return;
  else
  {
    if (number > 999) stp16cp05_send((number / 1000) % 10);
    else stp16cp05_send(30);								//space
    if (number > 99) stp16cp05_send((number / 100) % 10);
    else stp16cp05_send(30);								//space
    if (number > 9) stp16cp05_send((number / 10) % 10);
    else stp16cp05_send(30);								//space
    stp16cp05_send(number % 10);

    stp16cp05_latch();
  }
}


void stp16cp05_show_word(uint8_t chr1, uint8_t chr2, uint8_t chr3, uint8_t chr4)
{°
  if (chr1 < 31) stp16cp05_send(chr1);
  else stp16cp05_send(30);								//space
  if (chr2 < 31) stp16cp05_send(chr2);
  else stp16cp05_send(30);
  if (chr3 < 31) stp16cp05_send(chr3);
  else stp16cp05_send(30);
  if (chr4 < 31) stp16cp05_send(chr4);
  else stp16cp05_send(30);

  stp16cp05_latch();
}



void stp16cp05_send(uint16_t data)
{
  uint16_t temp;

  temp = conv_to_seven_segment(data);
  SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
  SPI.transfer((uint8_t) (temp / 256));
  SPI.transfer((uint8_t) (temp % 256));
  SPI.endTransaction();
}


uint16_t conv_to_seven_segment(uint8_t digit)
{
  if (digit > 30) return 0x0081;					//minus symbol
  else return (table[digit]);
}



void stp16cp05_latch(void)
{
  const int latch_pin = 8;

  digitalWrite(latch_pin, HIGH);
  delay(1);
  digitalWrite(latch_pin, LOW);
}
