# Big Seven Segment Display

Schematic and Kicad PCB design for a big and very bright seven segment display.
PCB size is 100mm x 50mm, the digit itself is approx 80mm x 50mm
The led driver is a STP16CP05 controlling 28 Cree JB5630 leds, it uses SPI.
Incl. arduino test program.
